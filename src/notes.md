# Retractable Gear notes

* Hydraulic system to extend and retract -- engine or electric driven
* Lights
  * 3x green -> gear extended
  * yellow -> gear in transit
* Squat switch prevents gear retraction when wheels on ground
* Pre-flight
  * Gear switch is down before turning on master
  * Master on
  * 3x green indicated
  * Gear wheel well clear of debris
  * Gear shocks inflated
  * Locking mechanism condition
  * Squat switch condition
* After take-off
  * At end of runway and +RoC
    * Tap brakes
    * Retract gear
    * Re-trim
* Emergency gear extension
  * releases hydraulic pressure
  * check circuit breakers (pulled for training purpose)
  * check panel lights
  * check gear indicator bulbs
  * airspeed < 86KIAS
  * move gear selector to down position
  * hold emergency gear lever in downward position
  * yaw aircraft side-to-side
  * if nose wheel failed to extend, slow to safest speed and move gear selector to down position
* Micro-switch warnings
  * Warning horn and a red warning light
  * MAP 14" or below and gear selector in up position
  * Flaps extended > 10 degrees without landing gear down and locked
  * Wheels on ground with gear selector in up position
* Workflows
  * BUMFISH and PUFF
  * When operating gear selector, keep hand on switch until 3x green
  * Re-trim for pitch changes
  * Gear down abeam threshold and before base
* Gear-up landing
  * Gear selector in up position
  * Pull circuit breaker
  * PAN PAN
  * Request visual inspection from ATC
  * Exhaust as much fuel as possible
  * Crossing threshold
    * mixture off
    * ignition off
    * master off
  * Unlatch prior to landing
  * Flare as much as possible
* After landing
  * check gear selector in down position
